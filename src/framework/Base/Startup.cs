﻿using Base.Extentions;
using Base.Services;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
    public abstract class StartupBase
    {
        protected readonly ISystemInfoService _SystemInfoService;

        protected readonly string _StartupLocation;
        protected readonly string _StartupDirectory;
        protected readonly ILogger _Logger;

        protected IConfiguration Configuration { get; set; }

        public StartupBase(IWebHostEnvironment env)
        {
            var assm = Assembly.GetEntryAssembly();
            _SystemInfoService = new SystemInfoService(assm);

            var loggerFactory = LoggerFactory.Create(b =>
            {
                b.AddConsole();
                if (_SystemInfoService.ProgramName == "testhost")
                    b.SetMinimumLevel(LogLevel.Error);
            });
            ILogger logger = loggerFactory.CreateLogger<StartupBase>();

            _StartupLocation = assm.Location;
            _StartupDirectory = Path.GetDirectoryName(_StartupLocation);
            var vaultJsonPath = "/vault/secrets";
            var basicFilePath = Path.Combine(_StartupDirectory, $"basic.{env.EnvironmentName.ToLower()}.json");
            logger.LogInformation($"Basic client config file path: {basicFilePath}");
            var builder = new ConfigurationBuilder()
                            .SetBasePath(env.ContentRootPath)
                            .AddJsonFile(basicFilePath, optional: env.IsDevelopment(), reloadOnChange: false)
                            .AddJsonFile("appsettings.json", optional: env.IsDevelopment(), reloadOnChange: false)
                            .AddJsonFile($"appsettings.{env.EnvironmentName.ToLower()}.json", optional: true, reloadOnChange: false);

            builder = builder.AddJsonFileForVault(vaultJsonPath, logger);
            builder = builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            _Logger = logger;
        }
        protected virtual void OnConfigureServices(IServiceCollection services, IMvcBuilder mvcBuilder)
        {

            services.AddSingleton<HostStartedResolver>();

            services.AddSingleton(_SystemInfoService);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddLocalization();
            services.AddSingleton(Configuration);
            services.AddControllers();

            services.AddMassTransit(x => x.UsingRabbitMq((context, cfg) =>
            {
                cfg.Host("localhost", "/", h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });
            }));
        }

        /// <summary>
        /// The localization middleware must be configured before any middleware which might check the request culture
        /// </summary>
        /// <param name="app"></param>
        protected void OnConfigureBase(IApplicationBuilder app)
        {
        }

        protected void OnConfigureBeforeUseMvc(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
           // TODO Migration start

            #region CORS
            if (env.IsDevelopment() || app.ApplicationServices.GetService<IConfiguration>().GetValue<bool>("CorsAllowAll"))
            {
                app.UseCors("AllowAll");
            }
            else
            {
                app.UseCors("AllowAdminClient");
            }
            #endregion
            #region Swagger
            var configuration = app.ApplicationServices.GetService<IConfiguration>();
            
            //if (env.IsDevelopment() || configuration.GetValue("UseSwagger", false))
            //{
            //    var basePath = configuration.GetValue<string>("BasePath");
            //    if (!string.IsNullOrEmpty(basePath)) app.UsePathBase(basePath);
            //    app.UseSwagger(c =>
            //    {

            //    });
            //    app.UseSwaggerUI(c =>
            //    {
            //        c.SwaggerEndpoint($"v1/swagger.json", $"{_SystemInfoService.Product} {_SystemInfoService.Version}");
            //    });
            //    app.UseReDoc(c =>
            //    {
            //        c.RoutePrefix = "docs";
            //        c.RequiredPropsFirst();
            //        c.SpecUrl($"{basePath}/swagger/v1/swagger.json");
            //    });
            //}
            #endregion

            app.UseAuthentication();
            app.UseRouting();
        }
        protected void OnConfigureAfterUseMvc(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {

            var logger = loggerFactory.CreateLogger(GetType());
            logger.LogInformation($"Service: {_SystemInfoService.ProgramName} started. Environment mode: {env.EnvironmentName}");

            app.ApplicationServices.GetService<HostStartedResolver>().HostStared();
        }

        protected virtual void OnAddSwaggerGen(SwaggerGenOptions options, OpenApiInfo info)
        {
        }

    }
}
