﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Base.Extentions
{
  public static class VaultExtension
  {
    public static IConfigurationBuilder AddJsonFileForVault(this IConfigurationBuilder builder, string path,  ILogger logger = null)
    {
      if (Directory.Exists(path))
      {
        foreach ( var subpath in Directory.EnumerateFiles(path))
        {
          builder = builder.AddJsonFile(Path.Combine(path, subpath),optional:false, reloadOnChange: true);
        }
      }
      else
      {
        logger?.LogInformation($"Vault configs not found in /vault/secrets/");
      }
      return builder;
    }
  }
}
