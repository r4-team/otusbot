﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace Base.Extentions
{
  public static class WebHostBuilderExtension
  {
    public static IWebHost ConfigureOtus<TStartup>(this IWebHostBuilder builder)
      where TStartup: StartupBase
    {
      return builder
        .ConfigureLogging((hostingContext, log) =>
        {
          var env = hostingContext.HostingEnvironment;
          log.ClearProviders();
          log.SetMinimumLevel(LogLevel.Trace);
          log.AddNLog($"nlog.{env.EnvironmentName.ToLower()}.config");
        })
        .UseStartup<TStartup>()
        .UseNLog()
        .Build();
    }
  }
}

