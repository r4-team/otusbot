﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
    public abstract class DefaultStartup : StartupBase
    {
        protected DefaultStartup(IWebHostEnvironment env)
            : base(env)
        {
        }
        protected abstract void OnAddServices(IServiceCollection services);
        protected virtual void OnUseCustom(IApplicationBuilder app) { }
        protected virtual void OnAddEndpoints(IEndpointRouteBuilder routes) { }
        protected abstract void OnAddDatabase(IServiceCollection services);
        protected abstract void OnUseDatabaseIndexes(IServiceProvider applicationServices);

        public void ConfigureServices(IServiceCollection services)
        {
            AddServices(services);
            AddDatabase(services);

            var builder = services
              .AddMvc()
              .AddApplicationPart(_SystemInfoService.Assembly);

            OnConfigureServices(services, builder);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            OnConfigureBase(app);
            UseDatabaseIndexes(app.ApplicationServices);
            UseCustom(app);
            OnConfigureBeforeUseMvc(app, env, loggerFactory);
            app.UseEndpoints(routes =>
            {
                AddEndpoints(routes);
                routes.MapControllers();
                routes.MapDefaultControllerRoute();
            });
            OnConfigureAfterUseMvc(app, env, loggerFactory);
        }

        private void AddServices(IServiceCollection services)
        {
            OnAddServices(services);
        }

        private void UseDatabaseIndexes(IServiceProvider applicationServices)
        {
            OnUseDatabaseIndexes(applicationServices);
        }

        private void AddDatabase(IServiceCollection services)
        {
            OnAddDatabase(services);
        }

        private void UseCustom(IApplicationBuilder app)
        {
            OnUseCustom(app);
        }

        private void AddEndpoints(IEndpointRouteBuilder routes)
        {
            OnAddEndpoints(routes);
        }
    }
}
