﻿using Base.Extentions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
  public static class MainHost
  {
    public static void Run<TStartup>(string[] args)
      where TStartup : StartupBase
    {
      try
      {
           WebHost.CreateDefaultBuilder(args)
          .UseDefaultServiceProvider(options => options.ValidateScopes = false)
          .ConfigureKestrel(k =>
          {
            var http_port = int.Parse(Environment.GetEnvironmentVariable("HTTP_PORT") ?? "80");
            var http2_port = int.Parse(Environment.GetEnvironmentVariable("HTTP2_PORT") ?? "81");

            k.ListenAnyIP(http_port, o => o.Protocols =
              HttpProtocols.Http1);
            k.ListenAnyIP(http2_port, o => o.Protocols =
              HttpProtocols.Http2);
          })
          .ConfigureOtus<TStartup>()
          .Run();



        Console.WriteLine($"Service {typeof(TStartup)} shutdown success.");
        System.Diagnostics.Debug.WriteLine($"Service {typeof(TStartup)} shutdown success.");
      }
      catch (SafeApplicationStopException ex)
      {
        Environment.ExitCode = Convert.ToInt32(ex.ExitCode);
#if DEBUG
        Console.WriteLine($"Safe application exception of {typeof(TStartup)}. Message: {ex.Message}");
#endif
        if (Environment.ExitCode != 0)
        {
          System.Diagnostics.Debug.WriteLine(ex);
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine($"Error running {typeof(TStartup)}. Message: {ex.Message}");
        Console.WriteLine(ex);
        System.Diagnostics.Debug.WriteLine(ex);
        throw;
      }
      finally
      {
        // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
        NLog.LogManager.Shutdown();
      }
    }
  }
}
