﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Base.Models
{
    public abstract class BaseDocument : IBaseDocument
    {
        public BaseDocument()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }
        public virtual DateTime? CreateDateTimeUtc { get; set; }
        public virtual DateTime? ModifyDateTimeUtc { get; set; }
        public virtual string Name { get; set; }
    }
}
