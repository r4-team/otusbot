﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Models
{
    public interface IBaseDocument
    {
        string Id { get; set; }
        DateTime? CreateDateTimeUtc { get; set; }
        DateTime? ModifyDateTimeUtc { get; set; }
        string Name { get; set; }
    }
}
