﻿namespace Base.Config
{
    public interface IMongoConnection
    {
        string ConnectionString { get; }
        string DatabaseName { get; }
    }

    /// <summary>
    /// Общее соединение для запросов к Mongodb 
    /// </summary>
    public class MongoConnection : IMongoConnection
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}