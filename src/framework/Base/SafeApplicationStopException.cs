﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base
{
  public class SafeApplicationStopException : Exception
  {
    public SafeApplicationStopException(int exitCode)
    {
      ExitCode = exitCode;
    }

    public int ExitCode { get; }

    public override string Message => $"Exit code: {ExitCode}";
  }
}
