﻿using Base.Models;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Data
{
    public interface IRepository<TObject>
        where TObject : class, IBaseDocument
    {
        public DbSet<TObject> Data { get; init; }
        public void SaveChanges();
    }

    public abstract class Repository<TObject>: DbContext, IRepository<TObject>
        where TObject : class, IBaseDocument
    {
        public Repository(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<TObject> Data {  get; init; }

        public void SaveChanges() => base.SaveChanges();
        
    }
}
