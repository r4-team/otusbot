﻿using Base.Config;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Base.Data
{
  public interface IMongoDataContext
  {
    IMongoDatabase GetDatabase();
    IMongoClient Client { get; }
  }
  public class MongoDataContext : IMongoDataContext
  {
    readonly IMongoClient _client;
    readonly IMongoDatabase _database;
    public MongoDataContext(IMongoConnection mongo) {
      var mongoConnectionUrl = new MongoUrl(mongo.ConnectionString);
      var mongoClientSettings = MongoClientSettings.FromUrl(mongoConnectionUrl);

      _client = new MongoClient(mongoClientSettings);
      _database = _client.GetDatabase(mongo.DatabaseName);
    }

    public  MongoDataContext(IOptions<MongoConnection> options) : this(options.Value){}

    public IMongoClient Client => _client;

    public IMongoDatabase GetDatabase()
    {
      return _database;
    }
  }
}
