﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Services
{
  class HostStartedResolver
  {
    public bool IsHostStared { get; private set; } = false;

    internal void HostStared()
    {
      IsHostStared = true;
    }
  }
}
