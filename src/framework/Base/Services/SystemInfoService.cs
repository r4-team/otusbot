﻿using System;
using System.Reflection;

namespace Base.Services
{
  public interface ISystemInfoService
  {
    string Version { get; }
    string Product { get; }
    string ProgramName { get; }
    string ProductBuildVersion { get; }
    string ProductVersion { get; }
    DateTime ProductBuildDateTime { get; }
    string ProductConfiguration { get; }
    string ProductCopyright { get; }
    string Description { get; }
    string GetDatabaseName();
    void SetDatabaseName(string databaseName);
    Assembly Assembly { get; }
  }
  class SystemInfoService : ISystemInfoService
  {
    public Assembly Assembly { get; }

    private readonly string _Version;
    private readonly string _Product;
    private readonly string _ProgramName;
    private readonly string _ProductBuildVersion;
    private readonly DateTime _ProductBuildDateTime;
    private readonly string _ProductVersion;
    private readonly string _ProductConfiguration;
    private readonly string _ProductCopyright;
    private readonly string _Description;
    private string _DatabaseName;

    public SystemInfoService(Assembly assm)
    {
      Assembly = assm;

      _Version = assm.GetCustomAttribute<AssemblyInformationalVersionAttribute>().InformationalVersion;
      _ProgramName = assm.GetName().Name;

      Assembly baseAssm = typeof(SystemInfoService).Assembly;
      var version = assm.GetName().Version;
      _Product = baseAssm.GetCustomAttribute<AssemblyProductAttribute>().Product;
      _ProductBuildVersion = version.ToString();
      _ProductBuildDateTime = new DateTime(2000, 1, 1).AddDays(version.Build).AddSeconds(version.Revision * 2);
      _ProductVersion = baseAssm.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion;
      _ProductConfiguration = baseAssm.GetCustomAttribute<AssemblyConfigurationAttribute>()?.Configuration;
      _ProductCopyright = baseAssm.GetCustomAttribute<AssemblyCopyrightAttribute>()?.Copyright;
      _Description = baseAssm.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description;

      if (string.IsNullOrEmpty(_ProgramName))
      {
        _DatabaseName = string.Empty;
      }
      else
      {
        _DatabaseName = _ProgramName.ToLower().Replace(".", "_");
      } 
    }
    public SystemInfoService() 
      : this(Assembly.GetEntryAssembly())
    {

    }
    
    public void SetDatabaseName(string databaseName)
    {
      _DatabaseName = databaseName;
    }

    public string Version => _Version;

    public string Product => _Product;

    public string ProgramName => _ProgramName;

    public string ProductBuildVersion => _ProductBuildVersion;

    public DateTime ProductBuildDateTime => _ProductBuildDateTime;

    public string ProductVersion => _ProductVersion;

    public string ProductConfiguration => _ProductConfiguration;

    public string ProductCopyright => _ProductCopyright;

    public string Description => _Description;

    public string GetDatabaseName()
    {
      return _DatabaseName;
    }
  }
}
