﻿
using Base.Data;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.EntityFrameworkCore.Extensions;
using TgBot.Domain.Entities;
using TgBot.Infrastucture.Data.Interfaces;

namespace TgBot.Infrastucture.Data
{
    public class AppUserRepository : Repository<AppUser>, IAppUserRepository
    {
        public AppUserRepository(DbContextOptions<AppUserRepository> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AppUser>()
                .ToCollection("app_users");
        }
    }

}

