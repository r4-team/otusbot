﻿using Base.Data;
using TgBot.Domain.Entities;

namespace TgBot.Infrastucture.Data.Interfaces
{
    public interface IAppUserRepository: IRepository<AppUser>
    {
    }
}
