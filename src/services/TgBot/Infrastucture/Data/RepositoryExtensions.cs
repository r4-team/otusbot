﻿using MongoDB.Driver;
using TgBot.Domain.Entities;

namespace TgBot.Infrastucture.Data
{
    public static class RepositoryExtensions
    {
        public static void UseMongoIndexes(this IServiceProvider provider)
        {

            var employeesCollection = provider.GetRequiredService<IMongoCollection<AppUser>>();
            employeesCollection.Indexes.CreateMany(new[]
            {
                new CreateIndexModel<AppUser>(Builders<AppUser>.IndexKeys.Ascending(x => x.Name)),
                new CreateIndexModel<AppUser>(Builders<AppUser>.IndexKeys.Ascending(x => x.TelegramId))
            });
        }
    }
}
