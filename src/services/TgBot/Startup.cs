﻿using Base;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using TgBot.Infrastucture.Data;
using TgBot.Infrastucture.Data.Interfaces;

namespace TgBot
{
    public class Startup : DefaultStartup
    {
        public Startup(IWebHostEnvironment env)
            : base(env)
        {
        }

        protected override void OnAddDatabase(IServiceCollection services)
        {
            var connectionString = Configuration.GetValue<string>("MONGODB_URI");
            if (connectionString == null)
            {
                Environment.Exit(0);
            }

            services.AddDbContext<AppUserRepository>(options => options.UseMongoDB(connectionString, "tgbot_api"));
            services.AddTransient<IAppUserRepository, AppUserRepository>();
        }

        protected override void OnUseDatabaseIndexes(IServiceProvider applicationServices)
        {
           // applicationServices.UseMongoIndexes();
        }

        protected override void OnAddServices(IServiceCollection services)
        {
        }
    }
}
