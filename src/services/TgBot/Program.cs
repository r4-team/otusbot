using TgBot;

namespace TgBot
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Base.MainHost.Run<Startup>(args);
        }
    }
}
