﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualBasic;
using System.Net;
using TgBot.Infrastucture.Data.Interfaces;

namespace TgBot.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AdminController : Controller
    {
        private readonly IAppUserRepository _appUserRepository;
        public AdminController(IAppUserRepository appUserRepository) 
        { 
            _appUserRepository = appUserRepository;
        }

        [HttpGet()]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAdminAsync()
        {
            _appUserRepository.Data.Add(new Domain.Entities.AppUser { Name = "Test", TelegramId = 22123454 });
            _appUserRepository.SaveChanges();

            return Ok("Test");
        }
    }
}
