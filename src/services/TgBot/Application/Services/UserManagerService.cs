﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TgBot.Domain.Entities;
using TgBot.Infrastucture.Data.Interfaces;

namespace TgBot.Application.Services
{
    public interface IUserManagerService
    {
        /// <summary>
        /// Получение пользователя по Телеграм ID
        /// </summary>
        /// <param name="telegramId"></param>
        /// <returns></returns>
        Task<AppUser> GetUserByTelegramIdAsync(long telegramId);

        /// <summary>
        /// Получение пользователя по нику
        /// </summary>
        /// <param name="username">Телеграм ник пользователя</param>
        /// <returns></returns>
        Task<AppUser> GetUserByUsernameAsync(string username);

        Task<AppUser> CreateUserByTelegramId(long telegramId);

        Task UpdateUserAsync(AppUser appUser);

        Task DeleteUserAsync(long telegramId);
        Task UpdateUserStateAsync(AppUser appUser);
    }

    public class UserManagerService : IUserManagerService
    {
        IAppUserRepository _AppUserRepository;

        public UserManagerService(IAppUserRepository appUserRepository)
        {
            _AppUserRepository = appUserRepository;
        }

        public async Task<AppUser> CreateUserByTelegramId(long telegramId)
        {
            AppUser appUser = new AppUser() { TelegramId = telegramId };
            await _AppUserRepository.Data.AddAsync(appUser);
            return appUser;
        }

        public Task DeleteUserAsync(long telegramId)
        {
            return _AppUserRepository.Data.Where(x => x.TelegramId == telegramId).ExecuteDeleteAsync();
        }

        public Task<AppUser> GetUserByTelegramIdAsync(long telegramId)
        {
            throw new NotImplementedException();
        }

        public Task<AppUser> GetUserByUsernameAsync(string username)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUserAsync(AppUser appUser)
        {
            throw new NotImplementedException();
        }

        public Task UpdateUserStateAsync(AppUser appUser)
        {
            throw new NotImplementedException();
        }
    }
}
