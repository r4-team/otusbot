﻿using Base.Models;
using Microsoft.EntityFrameworkCore;

namespace TgBot.Domain.Entities
{
    public class AppUser: BaseDocument
    {
        public string Name { get; set; }
        public required long TelegramId { get; set; }
    }
}
