﻿using StudyActivities.Domain.Entities;
using System.Net;

namespace StudyActivities.TestData
{
    public static class Seed
    {

        public static List<UserActivity> CreateUserActivities()
        {
            List<UserActivity> userActivities = new List<UserActivity>();
            List<string> users = new List<string>()
            {
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString(),
                Guid.NewGuid().ToString()
            };
           foreach (var item in users) 
            {
                userActivities.Add(
                        new UserActivity()
                        {
                            UserId = item,
                            ActivityType = Infrastucture.Enums.ActivityType.TgAnswer
                        });
                userActivities.Add(
                        new UserActivity()
                        {
                            UserId = item,
                            ActivityType = Infrastucture.Enums.ActivityType.TgUsefulLink
                        });
                userActivities.Add(
                        new UserActivity()
                        {
                            UserId = item,
                            ActivityType = Infrastucture.Enums.ActivityType.ZoomClassVisit
                        });
                userActivities.Add(
                        new UserActivity()
                        {
                            UserId = item,
                            ActivityType = Infrastucture.Enums.ActivityType.ZoomChat
                        });
                userActivities.Add(
                        new UserActivity()
                        {
                            UserId = item,
                           ActivityType = Infrastucture.Enums.ActivityType.ZoomWebcam
                        });
            }

           return userActivities;
        }
    }
}
