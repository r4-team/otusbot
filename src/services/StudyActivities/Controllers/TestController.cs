﻿using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using StudyActivities.Application.Servicies.Interfaces;
using StudyActivities.Domain.Entities;
using StudyActivities.Infrastucture.Enums;
using StudyActivities.Infrastucture.Interfaces;
using StudyActivities.TestData;
using System.Net;

namespace StudyActivities.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class TestController : Controller
    {
        private readonly IActivityManager _activityManager;
        public TestController(IActivityManager activityManager)
        {
            _activityManager = activityManager;
        }
        //если HttpPost то возвращаеть 405 ошибку
        [HttpGet("create")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> CreateActivities()
        {
            
            return Ok("not implemented");
        }

        [HttpGet("useractivities/{userId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetUserActivities(string userId)
        {
            var activities = await _activityManager.GetUserActivitiesByUserIdAsync(userId);
            return Ok($"{activities.Count}");
        }

        [HttpGet("activity/{activityId}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetActivityById(string activityId)
        {
            var activity = await _activityManager.GetActivityByIdAsync(activityId);
            return Ok($"Id: {activity.Id} UserId {activity.UserId} ActivityType {activity.ActivityType}");
        }

        [HttpGet("activityType/{type}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetActivityByType(ActivityType type)
        {
            var activities = await _activityManager.GetActivitiesByType(type);
            return Ok($"{activities.Count}");
        }

    }
}
