namespace StudyActivities
{
    class Program
    {
        public static void Main(string[] args) 
        {
            Base.MainHost.Run<Startup>(args);
        }
    }
}