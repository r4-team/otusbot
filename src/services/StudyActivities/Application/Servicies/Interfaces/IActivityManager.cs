﻿using StudyActivities.Domain.Entities;
using StudyActivities.Infrastucture.Enums;

namespace StudyActivities.Application.Servicies.Interfaces
{
    public interface IActivityManager
    {
        /// <summary>
        /// Получение активностей пользователя по его индетификатору в <see cref="AppUser"/>
        /// </summary>
        /// <param name="userId">Индентификатор пользователся из <see cref="AppUser"/></param>
        /// <returns>Список активностей пользователся</returns>
        Task<List<UserActivity>> GetUserActivitiesByUserIdAsync(string userId);
        /// <summary>
        /// Получение активности по индентификатору
        /// </summary>
        /// <param name="activityId">Индентификатор активности <see cref="UserActivity"/></param>
        /// <returns></returns>
        Task<UserActivity> GetActivityByIdAsync(string activityId);
        Task<List<UserActivity>> GetActivitiesByType(ActivityType type); 
        Task CreateActivityAsync(UserActivity activity);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="activityId"></param>
        /// <returns></returns>
        Task DeleteActivityAsync(string activityId);
    }
}
