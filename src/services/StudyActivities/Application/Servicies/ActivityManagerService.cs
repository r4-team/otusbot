﻿using Microsoft.EntityFrameworkCore;
using StudyActivities.Application.Servicies.Interfaces;
using StudyActivities.Domain.Entities;
using StudyActivities.Infrastucture.Enums;
using StudyActivities.Infrastucture.Interfaces;

namespace StudyActivities.Application.Servicies
{
    public class ActivityManagerService : IActivityManager
    {
        IActivityRepository _activityRepository;
        public ActivityManagerService(IActivityRepository activityRepository)
        {
             _activityRepository = activityRepository;
        }

        public async Task CreateActivityAsync(UserActivity activity)
        {
            await _activityRepository.Data.AddAsync(activity);
            _activityRepository.SaveChanges();
            return;
        }

        public Task DeleteActivityAsync(string activityId)
        {
            return Task.CompletedTask;
        }

        public async Task<List<UserActivity>> GetActivitiesByType(ActivityType type)
        {
            return await _activityRepository.Data.Where(x => x.ActivityType == type).ToListAsync();
        }

        public async Task<UserActivity> GetActivityByIdAsync(string activityId)
        {
            return await _activityRepository.Data.FirstAsync(x => x.Id == activityId);
        }

        public async Task<List<UserActivity>> GetUserActivitiesByUserIdAsync(string userId)
        {
            return await _activityRepository.Data.Where(x => x.UserId == userId).ToListAsync();
        }
    }
}
