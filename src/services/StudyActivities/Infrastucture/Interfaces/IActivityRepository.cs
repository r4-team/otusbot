﻿using Base.Data;
using StudyActivities.Domain.Entities;

namespace StudyActivities.Infrastucture.Interfaces
{
    public interface IActivityRepository : IRepository<UserActivity>
    {
    }
}
