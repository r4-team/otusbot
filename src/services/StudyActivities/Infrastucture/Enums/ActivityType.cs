﻿namespace StudyActivities.Infrastucture.Enums
{
    public enum ActivityType
    {
        TgAnswer,
        TgUsefulLink,
        ZoomClassVisit,
        ZoomWebcam,
        ZoomChat
    }
}
