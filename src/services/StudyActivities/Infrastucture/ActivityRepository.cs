﻿using Base.Data;
using Microsoft.EntityFrameworkCore;
using MongoDB.EntityFrameworkCore.Extensions;
using StudyActivities.Domain.Entities;
using StudyActivities.Infrastucture.Interfaces;

namespace StudyActivities.Infrastucture
{
    public class ActivityRepository : Repository<UserActivity>, IActivityRepository
    {
        public ActivityRepository(DbContextOptions options) : 
            base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UserActivity>().
                ToCollection("activities");
        }
    }
}
