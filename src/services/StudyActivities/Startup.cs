﻿using Base;
using MongoDB.Driver;
using Microsoft.EntityFrameworkCore;
using StudyActivities.Infrastucture;
using StudyActivities.Application.Servicies;
using StudyActivities.Application.Servicies.Interfaces;
using StudyActivities.Infrastucture.Interfaces;

namespace StudyActivities
{
    public class Startup : DefaultStartup
    {
        public Startup(IWebHostEnvironment env)
            : base(env)
        {
        }

        protected override void OnAddDatabase(IServiceCollection services)
        {
            string connectionString = Configuration.GetValue<string>("MongoConnectionString");
            if(connectionString == null)
            {
                Environment.Exit(0);
            }

            services.AddDbContext<ActivityRepository>(options => options.UseMongoDB(connectionString,"study_activities_api"));
            services.AddTransient<IActivityRepository, ActivityRepository>();
        }

        protected override void OnAddServices(IServiceCollection services)
        {
            services.AddTransient<IActivityManager, ActivityManagerService>();
        }

        protected override void OnUseDatabaseIndexes(IServiceProvider applicationServices)
        {
           
        }
    }
}
