﻿using Base.Models;
using StudyActivities.Infrastucture.Enums;

namespace StudyActivities.Domain.Entities
{
    public class UserActivity : BaseDocument
    {
        public ActivityType ActivityType { get; set; }
        public string UserId { get; set; }
    }
}
