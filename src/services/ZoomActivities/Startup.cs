﻿using Base;

namespace ZoomActivities
{
    public class Startup : DefaultStartup

    {
        public Startup(IWebHostEnvironment env)
            : base(env)
        {
        }

        protected override void OnAddDatabase(IServiceCollection services)
        {
            throw new NotImplementedException();
        }

        protected override void OnAddServices(IServiceCollection services)
        {
        }

        protected override void OnUseDatabaseIndexes(IServiceProvider applicationServices)
        {
            throw new NotImplementedException();
        }
    }
}
