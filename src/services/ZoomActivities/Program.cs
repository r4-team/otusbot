using Microsoft.AspNetCore.Hosting;
using ZoomActivities;

namespace ZoomActivities
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Base.MainHost.Run<Startup>(args);
        }
    }
}
